libmldbm-sync-perl (0.30-6) unstable; urgency=medium

  [ Debian Janitor ]
  * Apply multi-arch hints. + libmldbm-sync-perl: Add Multi-Arch: foreign.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sat, 19 Nov 2022 17:04:05 +0000

libmldbm-sync-perl (0.30-5) unstable; urgency=medium

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Laurent Baillet ]
  * fix lintian spelling-error-in-description warning

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Bump debhelper from deprecated 8 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set Testsuite header for perl package.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Wed, 15 Jun 2022 19:40:08 +0100

libmldbm-sync-perl (0.30-4.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Sun, 03 Jan 2021 12:59:54 +0100

libmldbm-sync-perl (0.30-4) unstable; urgency=low

  [ gregor herrmann ]
  * debian/control: Added: ${misc:Depends} to Depends: field.

  [ Nathan Handler ]
  * debian/watch: Update to ignore development releases.

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ gregor herrmann ]
  * debian/control: update {versioned,alternative} (build) dependencies.

  [ Salvatore Bonaccorso ]
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Axel Beckert ]
  * debian/copyright: migrate pre-1.0 format to 1.0 using "cme fix dpkg-
    copyright"

  [ gregor herrmann ]
  * Switch to "3.0 (quilt)" source format.
  * Use debhelper 8 and dh(1) in debian/rules.
  * debian/rules: re-create Makefile in clean target. (Closes: #724196)
  * debian/copyright: update formatting and years of packaging copyright.
  * Declare compliance with Debian Policy 3.9.4.

 -- gregor herrmann <gregoa@debian.org>  Sun, 22 Sep 2013 23:16:14 +0200

libmldbm-sync-perl (0.30-3) unstable; urgency=low

  * Take over for the Debian Perl Group; Closes: #344324 -- RFA
  * debian/control: Added: Vcs-Svn field (source stanza); Vcs-Browser
    field (source stanza); Homepage field (source stanza). Changed:
    Maintainer set to Debian Perl Group <pkg-perl-
    maintainers@lists.alioth.debian.org> (was: Debian QA Group
    <packages@qa.debian.org>); add /me to Uploaders.
  * Add debian/watch.
  * Don't install README any more (text version of the POD documentation).
  * debian/copyright: switch to new proposed format.
  * Set debhelper compatibility level to 7; adapt
    debian/{control,compat,rules}.
  * Add build dependency on libmldbm-perl (used in tests).
  * Add build dependency and Recommends on libtie-cache-perl.
  * debian/control: mention module name in long description.
  * Set Standards-Version to 3.8.0; separate Build-Depends and
    Build-Depends-Indep.

 -- gregor herrmann <gregoa@debian.org>  Sun, 31 Aug 2008 17:15:40 +0200

libmldbm-sync-perl (0.30-2) unstable; urgency=low

  * Orphan package, change maintainer to Debian QA Group
    <packages@qa.debian.org>
  * Add Depends: on libmldbm-perl (closes: Bug#344297)
  * Added debian/compat and removed DH_COMPAT from debian/rules
  * Change Section: from interpreters to perl

 -- Ivan Kohler <ivan-debian@420.am>  Wed, 21 Dec 2005 10:51:27 -0800

libmldbm-sync-perl (0.30-1) unstable; urgency=low

  * New upstream release
  * debian/copyright pedantry (closes: Bug#153342, Bug#153385)
  * binary-arch vs. binary-indep (closes: Bug#153360)

 -- Ivan Kohler <ivan-debian@420.am>  Sat,  7 Sep 2002 06:12:59 -0700

libmldbm-sync-perl (0.25-1) unstable; urgency=low

  * Initial Release (closes: Bug#134295).

 -- Ivan Kohler <ivan-debian@420.am>  Sat, 16 Feb 2002 12:34:56 -0800
